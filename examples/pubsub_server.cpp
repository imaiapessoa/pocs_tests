//
//  Weather update server in C++
//  Binds PUB socket to tcp://*:5556
//  Publishes random weather updates
//
//  Olivier Chamoux <olivier.chamoux@fr.thalesgroup.com>


#include <zmq.hpp>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <sstream>

#define within(num) (int) ((float) num * random () / (RAND_MAX + 1.0))

int main ()
{
    //  Prepare our context and publisher
    zmq::context_t context (1);
    zmq::socket_t publisher(context, ZMQ_PUB);

    publisher.bind("ipc:///tmp/zmq.ipc");
    std::string topic1 = "/app/weather";
    std::string topic2 = "/app/news";    



    //  Initialize random number generator
    srandom ((unsigned) time (NULL));
    while (1) {

        int zipcode, temperature, relhumidity;

        //  Get values that will fool the boss
        zipcode     = within (100000);
        temperature = within (215) - 80;
        relhumidity = within (50) + 10;

        
        std::stringstream ss_topic1;
        ss_topic1 << topic1 << " " 
                  << zipcode << " " 
                  << temperature << " " 
                  << relhumidity;

        std::cout << ss_topic1.str() << "\n" << std::endl;

        
        zmq::message_t message_topic1(ss_topic1.str().size());
        memcpy((char *)message_topic1.data(), (char *)ss_topic1.str().data(), ss_topic1.str().size() );
        //  Send message to all subscribers of topic1
        publisher.send(message_topic1);


        std::stringstream ss_topic2;
        ss_topic2 << topic2 << " " 
                  <<  "today weather : " << temperature;

        std::cout << ss_topic2.str() << "\n" << std::endl;
        zmq::message_t message_topic2(ss_topic2.str().size());
        memcpy((char *)message_topic2.data(), (char *)ss_topic2.str().data(), ss_topic2.str().size() );
        publisher.send(message_topic2);

        
        sleep(5);

    }
    
    return 0;
}