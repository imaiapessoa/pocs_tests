//
//  Weather update client in C++
//  Connects SUB socket to tcp://localhost:5556
//  Collects weather updates and finds avg temp in zipcode
//
//  Olivier Chamoux <olivier.chamoux@fr.thalesgroup.com>
//

#include <zmq.hpp>
#include <iostream>
#include <sstream>
#include <unistd.h>



int main (int argc, char *argv[])
{

   zmq::context_t context (1);

    //  Socket to talk to server
    std::cout << "Collecting updates from weather server...\n" << std::endl;
    zmq::socket_t subscriber (context, ZMQ_SUB);
    subscriber.connect("ipc:///tmp/zmq.ipc");
    
    std::string topic = (argc > 1) ? argv[1]: "/app/weather";
    subscriber.setsockopt(ZMQ_SUBSCRIBE, topic.c_str(), topic.size());    


    while (1) {

        zmq::message_t update;
        int zipcode, temperature, relhumidity;

        subscriber.recv(&update);
        std::istringstream iss(static_cast<char*>(update.data()));

        if (topic == "/app/weather") {        
           iss >> topic >> zipcode >> temperature >> relhumidity ;
           std::cout << "topic: " << topic << "\n" 
                  << "zipcode: " << zipcode << "\n" 
                  << "temperature:" << temperature << "\n"
                  << "relhumidity:" << relhumidity << "\n" << std::endl;
        }else {
           std::string news_msg;
           iss >> topic >> news_msg ;
           std::cout << "topic: " << topic << "\n" 
                  << "news_msg " << news_msg << "\n" << std::endl;
        }
    }

    return 0;
}
